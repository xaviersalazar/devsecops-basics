FROM node as build
RUN git clone  https://github.com/gothinkster/angular-realworld-example-app.git
WORKDIR /angular-realworld-example-app
RUN npm install
RUN npm run build

FROM ubuntu:20.10
RUN apt update -y
RUN apt install -y apache2
COPY --from=build /angular-realworld-example-app/dist/ /var/www/html/
CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]
